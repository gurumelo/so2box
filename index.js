var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var SerialPort 	= require('serialport');
var prependFile = require('prepend-file');

app.use(express.static('./public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var port = new SerialPort("/dev/ttyUSB0", {
	baudRate: 115200,
	parser: SerialPort.parsers.readline('\r\n')
});



var meses = ['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC'];

function formafecha(lafecha) {
	var fecha = new Date(lafecha);
	var horas = fecha.getHours();
	horas = ("0" + horas).slice(-2);
	var minutos = fecha.getMinutes();
	minutos = ("0" + minutos).slice(-2);
	var segundos = fecha.getSeconds();
	segundos = ("0" + segundos).slice(-2);
	fecha = fecha.getDate() +'-'+ meses[fecha.getMonth()] +'-'+ fecha.getFullYear() +' '+ horas +':'+ minutos +':'+ segundos;
	return fecha;
}


port.on('data', function (data) {
	var valores = data.split('#');

	if ( valores[0] == "E" ) {
		console.log('conectado');
	} else {
		var losvalores = {};
		losvalores.concentracion = valores[0];
		losvalores.temperatura = valores[1];
		losvalores.fecha = new Date();
		//console.log(losvalores);

		io.emit('v', losvalores);
		prependFile('public/d.txt', formafecha(losvalores.fecha) +';'+ losvalores.temperatura +';'+ losvalores.concentracion +'\n'); 
	}	

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
